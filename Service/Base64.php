<?php

namespace Apeisia\BaseBundle\Service;

abstract class Base64
{

    private static array $base64SubstitutionTable = [
        '+' => '-',
        '/' => '_',
        '=' => '',
    ];
    private static array $inverseBase64SubstitutionTable = [
        '-' => '+',
        '_' => '/',
    ];

    public static function toUrlSaveBase64($str): string
    {
        return strtr(base64_encode($str), self::$base64SubstitutionTable);
    }

    public static function fromUrlSaveBase64($b64)
    {
        return base64_decode(strtr($b64, self::$inverseBase64SubstitutionTable));
    }

}
