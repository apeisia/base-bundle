<?php

namespace Apeisia\BaseBundle\Service;

use Symfony\Component\HttpFoundation\HeaderUtils;

/**
 * Creates a Content-Disposition header, correctly handling special characters, spaces, utf-8 etc.
 */
abstract class ContentDispositionHelper
{
    public static function makeDisposition($fileName, $disposition = HeaderUtils::DISPOSITION_ATTACHMENT): string
    {
        $fileName         = str_replace(['/', '\\', '?', '%', '*', ':', '|', '"', '<', '>'], '_', $fileName);
        $fallbackFilename = iconv('utf-8', 'ascii//TRANSLIT', $fileName);

        return HeaderUtils::makeDisposition(
            $disposition,
            $fileName,
            $fallbackFilename
        );
    }
}
