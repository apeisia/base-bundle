<?php

namespace Apeisia\BaseBundle\Service;

use Apeisia\BaseBundle\Exception\ExpiredException;
use Apeisia\BaseBundle\Exception\MacVerifyFailedException;
use Apeisia\BaseBundle\Exception\MalformedMessageException;
use Apeisia\BaseBundle\Exception\WrongUseForException;
use Exception;

class EncryptAndMacService
{
    public function __construct(private readonly ?string $key)
    {
        if (!$key) {
            throw new Exception('you need to configure apeisia_base.signed_entity_key');
        }
        sodium_crypto_secretbox_keygen();
    }

    public function encrypt(string $useFor, mixed $data, \DateTimeImmutable $validUntil = null): string
    {
        if (str_contains($useFor, '|')) {
            throw new \LogicException("don't use | in useFor");
        }
        $message = $useFor . '|';
        if ($validUntil) {
            $message .= $validUntil->format('Y-m-d H:i:s');
        }
        $message .= '|' . json_encode($data);

        $message = gzcompress($message, 9);


        $salt = random_bytes(SODIUM_CRYPTO_PWHASH_SALTBYTES);
        $key = $this->deriveKeyFromUserPassword($salt);
        $nonce = random_bytes(SODIUM_CRYPTO_SECRETBOX_NONCEBYTES);
        $ciphertext = sodium_crypto_secretbox($message, $nonce, $key);

        sodium_memzero($key);

        return Base64::toUrlSaveBase64($salt . $nonce . $ciphertext);

    }

    public function verify(string $useFor, mixed $payload): bool
    {
        try {
            $this->decrypt($useFor, $payload);
            return true;
        } catch (MacVerifyFailedException|MalformedMessageException|WrongUseForException $e) {
            return false;
        }
    }

    public function decrypt(string $useFor, string $base64Text): mixed
    {
        $ciphertext = Base64::fromUrlSaveBase64($base64Text);
        $salt = substr($ciphertext, 0, SODIUM_CRYPTO_PWHASH_SALTBYTES);
        $nonce = substr($ciphertext, SODIUM_CRYPTO_PWHASH_SALTBYTES, SODIUM_CRYPTO_SECRETBOX_NONCEBYTES);
        $ciphertext = substr($ciphertext, SODIUM_CRYPTO_PWHASH_SALTBYTES + SODIUM_CRYPTO_SECRETBOX_NONCEBYTES);
        $key = $this->deriveKeyFromUserPassword($salt);

        $message = sodium_crypto_secretbox_open($ciphertext, $nonce, $key);
        sodium_memzero($key);

        if ($message === false) {
            throw new MacVerifyFailedException();
        }

        $message = gzuncompress($message);

        $e = explode('|', $message, 3);
        if (count($e) != 3) {
            throw new MalformedMessageException();
        }
        if ($e[0] != $useFor) {
            throw new WrongUseForException();
        }
        if (strlen($e[1]) > 0 && new \DateTimeImmutable($e[1]) < new \DateTimeImmutable()) {
            throw new ExpiredException();
        }
        return json_decode(
            $e[2],
            true
        );
    }


    private function deriveKeyFromUserPassword(string $salt): string
    {
        return sodium_crypto_pwhash(
            SODIUM_CRYPTO_SECRETBOX_KEYBYTES,
            $this->key,
            $salt,
            SODIUM_CRYPTO_PWHASH_OPSLIMIT_INTERACTIVE,
            SODIUM_CRYPTO_PWHASH_MEMLIMIT_INTERACTIVE
        );
    }
}
