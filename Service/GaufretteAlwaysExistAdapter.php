<?php

namespace Apeisia\BaseBundle\Service;

use Gaufrette\Adapter;
use Gaufrette\Adapter\Local;
use SplFileInfo;

class GaufretteAlwaysExistAdapter implements Adapter
{
    private Local $localAdapter;

    public function __construct(
        $directory,
        $create = false,
        $mode = 0777
    )
    {

        $this->localAdapter = new Local($directory, $create, $mode);
    }

    public function read($key)
    {
        if ($this->localAdapter->exists($key)) {
            return $this->localAdapter->read($key);
        }

        $info     = new SplFileInfo($key);
        $fallback = __DIR__ . '/../Resources/AlwaysExistFallbackFiles/fallback.' . $info->getExtension();
        if (file_exists($fallback)) {
            return file_get_contents($fallback);
        }
        return '';
    }

    public function write($key, $content)
    {
        return $this->localAdapter->write($key, $content);
    }

    public function exists($key)
    {
        return true;
    }

    public function keys()
    {
        return $this->localAdapter->keys();
    }

    public function mtime($key)
    {
        if ($this->localAdapter->exists($key)) {
            return $this->localAdapter->mtime($key);
        }
        return 0;
    }

    public function delete($key)
    {
        if ($this->localAdapter->exists($key)) {
            return $this->localAdapter->delete($key);
        }
        return true;
    }

    public function rename($sourceKey, $targetKey)
    {
        if ($this->localAdapter->exists($sourceKey)) {
            return $this->localAdapter->rename($sourceKey, $targetKey);
        } else {
            // copy fallback file to dest
            return $this->write($targetKey, $this->read($sourceKey));
        }
    }

    public function isDirectory($key)
    {
        return $this->localAdapter->isDirectory($key);
    }
}