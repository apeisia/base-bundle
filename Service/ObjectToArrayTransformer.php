<?php

namespace Apeisia\BaseBundle\Service;

use JMS\Serializer\ArrayTransformerInterface;
use JMS\Serializer\SerializationContext;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

if (interface_exists('JMS\Serializer\ArrayTransformerInterface')) {
    class ObjectToArrayTransformer
    {
        private ArrayTransformerInterface $arrayTransformer;

        public function __construct(ArrayTransformerInterface $arrayTransformer)
        {
            $this->arrayTransformer = $arrayTransformer;
        }

        public function toArray($object, array $groups): array
        {
            $context = SerializationContext::create();
            $context->setGroups($groups);

            return $this->arrayTransformer->toArray($object, $context);
        }
    }
} else {
    class ObjectToArrayTransformer
    {
        public function __construct(private readonly NormalizerInterface $normalizer)
        {
        }

        public function toArray($object, array $groups): array
        {
            return $this->normalizer->normalize($object, null, $groups);
        }
    }
}
