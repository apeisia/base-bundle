<?php

namespace Apeisia\BaseBundle\Service;

use Apeisia\BaseBundle\Event\AppConfigEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class AppConfigService
{
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    public function getAppConfig(): array
    {
        $event = new AppConfigEvent();

        $this->eventDispatcher->dispatch($event);

        return $event->getConfig();
    }
}
