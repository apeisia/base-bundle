<?php

namespace Apeisia\BaseBundle\Service\VueForm;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

interface SchemaGeneratorInterface
{

    public function createResponse(FormInterface $form): JsonResponse;

    public function createSchema(FormInterface $form): array;
}