<?php

namespace Apeisia\BaseBundle\Service\VueForm;

use InvalidArgumentException;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Contracts\Cache\ItemInterface;
use Symfony\Contracts\Cache\TagAwareCacheInterface;

class CachedFormSchemaBuilder
{
    private string $cacheKey;
    private array $cacheTags;

    public function __construct(
        private readonly SchemaGeneratorInterface $schemaGenerator,
        private readonly TagAwareCacheInterface   $cache,
        private readonly FormInterface            $form,
    )
    {
    }

    /**
     * Define the cache key for this form. Usually the FQCN of the form type.
     */
    public function cacheBy(string $cacheKey): self
    {
        $this->cacheKey = $this->sanitizeCacheKey($cacheKey);

        return $this;
    }

    /**
     * Adds one or multiple cache tags to the cache item. Usually the FQCN of the entities used in the form, i.e.
     * for selects etc. To only tag for a specific entity id, use the entity FQCN, followed by two underscores and the
     * entity id.
     */
    public function cacheTags(array|string $tags): self
    {
        if (is_string($tags)) {
            $tags = [$tags];
        }

        foreach ($tags as $tag) {
            $this->cacheTags[] = $this->sanitizeCacheKey($tag);
        }

        return $this;
    }

    /**
     * Get the schema as an array.
     */
    public function getSchema(): array
    {
        if (!$this->cacheKey) {
            throw new InvalidArgumentException('You need to provide a cache key when passing an instance of FormInterface');
        }

        return $this->cache->get($this->cacheKey, function (ItemInterface $cacheItem) {
            $cacheItem->tag($this->cacheTags);

            return $this->schemaGenerator->createSchema($this->form);
        });
    }

    /**
     * Get the schema as a JsonResponse.
     */
    public function getResponse(): JsonResponse
    {
        return new JsonResponse($this->getSchema());
    }

    private function sanitizeCacheKey(string $cacheKey): string
    {
        return str_replace(['{', '}', '(', ')', '/', '@', '\\'], '_', $cacheKey);
    }
}
