<?php

namespace Apeisia\BaseBundle\Service\VueForm;

use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\View\View;
use LogicException;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormErrorIterator;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class VueForm
{
    private FormInterface $form;
    private EntityManagerInterface $em;
    private SchemaGeneratorInterface $formSchemaGenerator;

    public function __construct(
        FormInterface            $form,
        EntityManagerInterface   $em,
        SchemaGeneratorInterface $formSchemaGenerator,
    )
    {
        $this->form                = $form;
        $this->em                  = $em;
        $this->formSchemaGenerator = $formSchemaGenerator;
    }

    public function createSchemaResponse(): JsonResponse
    {
        return $this->formSchemaGenerator->createResponse($this->form);
    }

    public function save(Request $request, ?callable $persistAction = null, ?callable $responseAction = null): View
    {
        $this->submit($request);

        if ($this->isValid()) {
            $entity = $this->form->getData();

            if (is_callable($persistAction)) {
                $persistAction($this->form, $entity, $this->em);
            } else {
                $this->em->persist($entity);
            }

            // $persistAction can invalidate the form
            if ($this->form->isValid()) {
                $this->em->flush();

                if (is_callable($responseAction)) {
                    return $responseAction($this->form, $entity);
                }

                if (method_exists($entity, 'getId')) {
                    return View::create(['id' => $entity->getId()], Response::HTTP_OK);
                }

                return View::create('', Response::HTTP_NO_CONTENT);
            }
        }

        return $this->createFormErrorView();
    }

    public function submit(Request $request): void
    {
        $this->form->submit($this->getRequestBody($request));
    }

    public function isValid(): bool
    {
        return $this->form->isValid();
    }

    public function createFormErrorView(): View
    {
        // first, use deep errors for easier error message extraction
        $deepErrors = $this->serializeErrorIterator($this->form->getErrors(true, false));
        // flatten
        $flatErrors = [];
        array_walk_recursive($deepErrors, function ($value, $key) use (&$flatErrors) {
            $flatErrors[$key] = $value;
        });

        return View::create(
            $deepErrors,
            Response::HTTP_UNPROCESSABLE_ENTITY
        );
    }

    private function serializeErrorIterator(FormErrorIterator $errorIterator, string $prefix = ''): array|string
    {
        // $errorIterator iterates over FormError and FormErrorIterator.
        // an FormErrorIterator indicates a subform, but we need a flat array

        $errorIsIterator = function ($error) {
            $subIteratorCount = count(filter($error, fn($item) => $item instanceof FormErrorIterator));
            // sanity check
            if ($subIteratorCount != 0 && $subIteratorCount != count($error)) {
                // all children need to be FormError OR FormErrorIterator
                // throw new LogicException('Unexpected mix of FormError and FormErrorIterator');
            }

            return $subIteratorCount != 0;
        };

        $out = ['_' => []];
        // all iterators
        foreach ($errorIterator as $item) {
            if ($item instanceof FormError) { // form-wide error
                $out['_'][] = $item->getMessage();
            } else if ($errorIsIterator($item)) {
                // flatten subform with prefix
                $out += $this->serializeErrorIterator($item, $prefix . $item->getForm()->getName() . '.');
            } else {
                $out[$prefix . $item->getForm()->getName()] = $this->serializeErrorMessages($item);
            }
        }

        return $out;

    }

    private function serializeErrorMessages(FormErrorIterator $errorIterator): array
    {
        $out = [];
        foreach ($errorIterator as $item) {
            if (!($item instanceof FormError)) {
                throw new LogicException('Expected FormError');
            }
            $out[] = $item->getMessage();
        }

        return array_values(array_unique($out));
    }

    private function getRequestBody(Request $request): array
    {
        $params = $request->request->all();
        $files  = $request->files->all();

        unset($params['id']);
        unset($params['_method']);

        return array_replace_recursive($params, $files);
    }

    public function get(string $name): FormInterface
    {
        return $this->form->get($name);
    }

    public function getForm(): FormInterface
    {
        return $this->form;
    }
}
