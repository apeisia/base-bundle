<?php

namespace Apeisia\BaseBundle\Service\VueForm;

use InvalidArgumentException;
use LogicException;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Contracts\Cache\TagAwareCacheInterface;

readonly class CachedFormSchemaFactory
{
    public function __construct(
        private SchemaGeneratorInterface                                              $schemaGenerator,
        private FormFactoryInterface                                                  $symfonyFormFactory,
        #[Autowire(service: "cache.form_schema")] private TagAwareCacheInterface|null $cache = null,
    )
    {
    }

    /**
     * Creates the schema for the given entity. The schema is cached and returned as a JsonResponse.
     * Use a unique $cacheKey for each form, which can simply be the FQCN of the form type. You can also provide tags,
     * which can be used to invalidate the cache. The convention is to use FQCN of the entities used in the form
     * (for selects etc.) as tags. To only tag for a specific entity id, use the entity FQCN, followed by two underscores
     * and the entity id. If no cache key is given, the FQCN of the form type is used.
     *
     * Examples for cache keys or tags:
     *   AppBundle\Form\UserType
     *   AppBundle\Entity\User
     *   AppBundle\Entity\User__2921ab26-6e00-40da-9fe4-9fa527b7eed3
     */
    public function form(
        string|FormInterface $form,
        mixed                $data = null,
        array                $options = null,
    ): CachedFormSchemaBuilder
    {
        if (!$this->cache) {
            throw new LogicException('To use cached schema, you need to configure the "cache.form_schema" cache pool.');
        }

        if ($form instanceof FormInterface && $data !== null) {
            throw new InvalidArgumentException('You cannot pass $data when you pass an instance of FormInterface instead of an form class name');
        }

        if ($form instanceof FormInterface && $options !== null) {
            throw new InvalidArgumentException('You cannot pass $options when you pass an instance of FormInterface instead of an form class name');
        }

        $cacheKey = null;
        if (is_string($form)) {
            $cacheKey ??= $form;
            $form     = $this->symfonyFormFactory->create($form, $data, $options ?? []);
        }

        assert($form instanceof FormInterface);

        $builder = new CachedFormSchemaBuilder($this->schemaGenerator, $this->cache, $form);
        if ($cacheKey) {
            $builder->cacheBy($cacheKey);
        }

        return $builder;
    }

}
