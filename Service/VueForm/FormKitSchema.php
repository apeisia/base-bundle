<?php

namespace Apeisia\BaseBundle\Service\VueForm;

use Apeisia\BaseBundle\Validator\FormkitFrontendConstraint;
use JetBrains\PhpStorm\ArrayShape;
use Symfony\Component\Form\ChoiceList\View\ChoiceGroupView;
use Symfony\Component\Form\ChoiceList\View\ChoiceView;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\NotBlank;

class FormKitSchema implements SchemaGeneratorInterface
{
    public function __construct(
        private readonly FormFactoryInterface $formFactory
    )
    {
    }

    public function createResponse(FormInterface $form): JsonResponse
    {
        return new JsonResponse($this->createSchema($form));
    }

    public function createSchema(FormInterface $form): array
    {
        return $this->walkForm($form);
    }

    private function walkForm(FormInterface $form): array
    {
        $out = [];

        $map = [
            'datetime'          => 'datetime-local',
            'collection'        => 'list',
            'translations_form' => 'translations',
        ];

        $choiceTypes = ['choice', 'entity', 'country', 'enum'];

        foreach ($form->all() as $child) {
            $view = $child->createView();
            $vars = $view->vars;
            $type = $vars['block_prefixes'][count($vars['block_prefixes']) - 2];

            $type = $map[$type] ?? $type;

            if ($type == 'list') {
                $config = $child->getConfig();
                $entryForm = $this->formFactory->createNamed($config->getOption('prototype_name'), $config->getOption('entry_type'));
                $schema    = [
                    //'$formkit' => 'list',
                    '$cmp'     => 'VfFormCollection',
                    'name'     => $vars['name'],
                    'children' => $this->walkForm($entryForm),
                    'props'    => [
                        'name'                => $vars['name'],
                        'overrideDefaultSlot' => true,
                    ],
                ];
            } elseif ($type == 'translations') {
                $config = $child->getConfig();
                $entryForm = $this->formFactory->createNamed($config->getOption('prototype_name'), $config->getOption('entry_type'));
                $schema = [
                    '$cmp'  => 'TranslationsForm',
                    'name'  => $vars['name'],
                    'props' => [
                        'name'       => $vars['name'],
                        'horizontal' => $vars['horizontal'],
                        'schema'     => $this->walkForm($entryForm),
                    ],
                ];
            } elseif (!in_array($type, $choiceTypes) && $child->all()) {
                $schema = [
                    '$formkit' => 'group',
                    'name'     => $vars['name'],
                    'children' => $this->walkForm($child)
                ];
            } else {
                $label = $vars['label'] ?? ucfirst(strtolower(trim(preg_replace(['/([A-Z])/', '/[_\s]+/'], ['_$1', ' '], $vars['name'])))); // "humanize" twig filter
                if (in_array($type, $choiceTypes)) {
                    $schema = $this->transformChoice($vars);
                } else {
                    $schema = match ($type) {
                        'integer' => [
                            '$formkit' => 'number',
                            'step'     => $vars['attr']['step'] ?? 1
                        ],
                        'price_cent' => [
                            '$formkit' => 'number',
                            'step'     => 0.01,
                            'min'      => 0,
                            'factor'   => 100,
                        ],
                        default => ['$formkit' => $type],
                    };
                }
                $schema = array_merge($schema, [
                    'name'  => $vars['name'],
                    'id'    => $vars['name'],
                    'label' => $label,
                    'help'  => $vars['help'],
                    ...$vars['attr'],
                ]);

                $validator = $this->transformValidator($child);
                if ($validator) {
                    $schema['validation'] = $validator;
                }
                $validatorMessages = $this->transformValidatorMessages($child);
                if ($validatorMessages) {
                    $schema['validationMessages'] = $validatorMessages;
                }
            }

            if (array_key_exists('disabled', $vars) && $vars['disabled']) {
                $schema['disabled'] = true;
            }

            $out[] = $schema;
        }
        return $out;
    }

    #[ArrayShape(['options' => "array", '$formkit' => "string", 'multiple' => "bool|undefined"])]
    private function transformChoice(array $vars): array
    {
        $multiple = $vars['multiple'];
        $expanded = $vars['expanded'];
        $choices = $vars['choices'];
        $empty = [
            'value' => '',
            'label' => '',
        ];
        $config = [
            'options' => $vars['required'] || $expanded ? [] : [$empty],
        ];
        foreach ($choices as $choice) {
            if ($choice instanceof ChoiceView) {
                $config['options'][] = [
                    'value'          => $choice->value,
                    'label'          => $choice->label,
                    'additionalData' => $choice->attr['additional_data'] ?? null,
                    'if'             => '1 == 0',
                ];
            } else if ($choice instanceof ChoiceGroupView) {
                $config['options'][] = [
                    'label' => $choice->label,
                    'value' => '_______formkit_hide_______',
                ];
                foreach ($choice->choices as $subChoice) {

                    $config['options'][] = [
                        'value'          => $subChoice->value,
                        'label'          => $subChoice->label,
                        'additionalData' => $subChoice->attr['additional_data'] ?? null
                    ];
                }
            }
        }

        if ($expanded) {
            $config['$formkit'] = $multiple ? 'checkbox' : 'radio';
        } else {
            $config['$formkit'] = 'select';
            if ($multiple) {
                $config['multiple'] = true;
            }
        }

        return $config;
    }

    private function transformValidator(FormInterface $form): string
    {
        $validators = map(
            $form->getConfig()->getOption('constraints') ?? [],
            function (Constraint $constraint) {
                return match (true) {
                    $constraint instanceof NotBlank => 'required',
                    $constraint instanceof FormkitFrontendConstraint => $constraint->getFormkitValidator() . ($constraint->getFormkitValidatorParam() ? ':' . $constraint->getFormkitValidatorParam() : ''),
                    default => null,
                };
            }
        );
        if ($form->getConfig()->hasOption('required')) {
            $required = $form->getConfig()->getOption('required');
            if ($required) {
                $validators[] = 'required';
            } else if (in_array('required', $validators)) {
                // we have a backend validation, but frontend validation is explicitly disabled
                $validators = array_diff($validators, ['required']);
            }
        }

        return join('|', filter(array_unique($validators), fn($i) => $i != null));
    }

    private function transformValidatorMessages(FormInterface $form): array
    {
        $messages = [];
        foreach ($form->getConfig()->getOption('constraints') ?? [] as $constraint) {
            if ($constraint instanceof FormkitFrontendConstraint && $constraint->getMessage()) {
                $messages[$constraint->getFormkitValidator()] = $constraint->getMessage();
            }
        }

        return $messages;
    }
}
