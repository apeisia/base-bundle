<?php

namespace Apeisia\BaseBundle\Service\VueForm;

use Doctrine\ORM\EntityManagerInterface;
use InvalidArgumentException;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;

class VueFormFactory
{
    private FormFactoryInterface $formFactory;
    private SchemaGeneratorInterface $formSchemaGenerator;
    private EntityManagerInterface $em;

    public function __construct(
        FormFactoryInterface     $formFactory,
        SchemaGeneratorInterface $formSchemaGenerator,
        EntityManagerInterface   $em,
    )
    {
        $this->formFactory         = $formFactory;
        $this->formSchemaGenerator = $formSchemaGenerator;
        $this->em                  = $em;
    }

    private function createForm(FormInterface|string $form, mixed $data = null, array $options = []): FormInterface
    {
        if ($form instanceof FormInterface) {
            if ($data !== null) {
                throw new InvalidArgumentException('You cannot pass $data when you pass an instance of FormInterface instead of an form class name');
            }
            if (count($options) > 0) {
                throw new InvalidArgumentException('You cannot pass $options when you pass an instance of FormInterface instead of an form class name');
            }

            return $form;
        }

        return $this->formFactory->create($form, $data, $options);
    }

    public function createVueForm(
        FormInterface|string $form,
        mixed                $data = null,
        array                $options = []
    ): VueForm
    {
        return new VueForm(
            $this->createForm($form, $data, $options),
            $this->em,
            $this->formSchemaGenerator,
        );
    }
}
