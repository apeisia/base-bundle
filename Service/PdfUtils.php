<?php

namespace Apeisia\BaseBundle\Service;

use Gedmo\Uploadable\MimeType\MimeTypeGuesser;
use Imagick;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class PdfUtils
{
    private static array $tmpFiles = []; // hold references until script ends

    public static function unite(array $files, string $destination = null): string
    {
        if (!$destination) {
            $file             = tmpfile();
            self::$tmpFiles[] = $file;
            $destination      = stream_get_meta_data($file)['uri'];
        }

        $tmpFiles = [];

        foreach ($files as &$file) {

            $tmpFile    = tmpfile();
            $tmpFiles[] = $tmpFile;
            $newFile    = stream_get_meta_data($tmpFile)['uri'];

            if (is_resource($file)) {
                $file = new File(stream_get_meta_data($file)['uri']);
            }

            if ($file instanceof File) {
                $file = $file->getPathname();
            }

            self::normalizePathToPdf($file, $newFile, true);

            $file = $newFile;
        }

        $process = new Process(
            [
                './pdfunite',
                ...$files,
                $destination
            ],
            __DIR__ . '/../../pdfunite-bin',
        );
        $process->run();

        foreach ($tmpFiles as $tmpFile) {
            fclose($tmpFile);
        }

        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        return $destination;
    }

    /**
     * Runs unite([$inputPath], $outputPath) to generate a PDF with a version that FPDF understands.
     *
     * @param string $inputPath
     * @param string $outputPath
     * @return string
     */
    public static function convertVersion(string $inputPath, string $outputPath): string
    {
        return self::unite([$inputPath], $outputPath);
    }

    public static function normalizePathToPdf(string $input, ?string $output, bool $convertToA4 = false): string
    {
        if (!$output) {
            $file             = tmpfile();
            self::$tmpFiles[] = $file;
            $output           = stream_get_meta_data($file)['uri'];
        }
        $mime = (new MimeTypeGuesser)->guess($input);

        if ($mime != 'application/pdf') {
            // not a pdf. try to convert it to one
            $pdf = new Imagick($input);

            if ($convertToA4) {
                // calculate a resolution that makes the image fit into the pdf without scaling
                $PDF_WIDTH  = 21.6; // try&error values to match a4 paper size
                $PDF_HEIGHT = 27.95;

                $geometry   = $pdf->getImageGeometry();
                $resolution = max($geometry['width'] / $PDF_WIDTH, $geometry['height'] / $PDF_HEIGHT) + 4;

                $width  = $PDF_WIDTH * $resolution;
                $height = $PDF_HEIGHT * $resolution;

                $pdf->setImageUnits(Imagick::RESOLUTION_PIXELSPERCENTIMETER);
                $pdf->setResolution($resolution, $resolution);
                $pdf->setPage($width, $height, $PDF_WIDTH, $PDF_HEIGHT);
            }

            $pdf->setImageFormat('pdf');
            $pdf->writeImage($output);
        } else {
            copy($input, $output);
        }

        return $output;
    }

    private static function getExtensionFromFilePath(string $filePath): ?string
    {
        $filePathParts = explode('.', $filePath);

        return array_pop($filePathParts);
    }
}
