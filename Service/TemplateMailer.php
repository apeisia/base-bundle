<?php

namespace Apeisia\BaseBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;
use Twig\Environment;

class TemplateMailer
{
    public function __construct(
        private MailFromHelper  $mailFromHelper,
        private MailerInterface $mailer,
        private Environment     $twig
    )
    {
    }

    public function send($template, $to, $context = [], $mailFrom = null, $mailSender = null): void
    {
        $this->mailer->send($this->createMessage($template, $to, $context, $mailFrom, $mailSender));
    }

    public function createMessage($template, $to, $context = [], $mailFrom = null, $mailSender = null): Email
    {
        if (!$mailFrom) {
            $mailFrom = $this->mailFromHelper->getFromAddress();
        }

        if (!$mailSender) {
            $mailSender = $this->mailFromHelper->getMailSenderName();
        }

        $tpl     = $this->twig->load($template);
        $subject = $tpl->renderBlock('subject', $context);

        $msg = new Email();
        $msg->to($to);
        $msg->from(new Address($mailFrom, $mailSender));
        $msg->subject($subject);

        if ($tpl->hasBlock('body')) {
            $body = $tpl->renderBlock('body', $context);
            if (!empty($body)) {
                $msg->text($body);
            }
        }

        if ($tpl->hasBlock('html')) {
            $html = $tpl->renderBlock('html', $context);
            if (!empty($html)) {
                $msg->html($html);
            }
        }

        return $msg;
    }
}
