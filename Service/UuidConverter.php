<?php

namespace Apeisia\BaseBundle\Service;

use Exception;
use Tuupola\Base85;

abstract class UuidConverter
{

    public static function toBin($id): string
    {
        return hex2bin(str_replace('-', '', $id));
    }

    public static function toHex($bin): string
    {
        $hex = bin2hex($bin);
        if (strlen($hex) != 32) throw new Exception('could not convert to uuid: decoded value is not not 32 bytes');

        return
            substr($hex, 0, 8) . '-' .
            substr($hex, 8, 4) . '-' .
            substr($hex, 12, 4) . '-' .
            substr($hex, 16, 4) . '-' .
            substr($hex, 20, 12);
    }

    public static function toBase85($id): string
    {
        return (new Base85())->encode(self::toBin($id));
    }

    public static function fromBase85($b85): string
    {
        return self::toHex((new Base85())->decode($b85));
    }

    public static function toBase64($id): string
    {
        return Base64::toUrlSaveBase64(self::toBin($id));
    }

    public static function fromBase64($b64): string
    {
        return self::toHex(Base64::fromUrlSaveBase64($b64));
    }

    public static function isUUID($uuid) {
        return preg_match('/^[0-9A-F]{8}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{12}$/i', $uuid);
    }
}
