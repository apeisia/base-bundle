<?php

namespace Apeisia\BaseBundle\Service;

use DateTimeInterface;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use InvalidArgumentException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Uid\Ulid;

class SignedEntityToken
{
    private ?string $key;

    private const BASE64_ENCODED_UUID_LENGTH = 22;
    private const BASE64_ENCODED_HMAC_LENGTH = 43;
    private const HEX_ENCODED_VALID_UNTIL_LENGTH = 8;
    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em, ?string $key)
    {
        if (!$key) {
            throw new Exception('you need to configure apeisia_base.signed_entity_key');
        }
        $this->key = $key;
        $this->em = $em;
    }

    public function generateToken(object|string $entityOrUUID, string $useFor, DateTimeInterface|int $validUntil = null): string
    {
        if ($validUntil instanceof DateTimeInterface) {
            $validUntil = $validUntil->getTimestamp();
        }
        $validUntil = $validUntil ? dechex($validUntil) : '';
        if ($validUntil != '' && strlen($validUntil) != self::HEX_ENCODED_VALID_UNTIL_LENGTH) {
            throw new InvalidArgumentException('encoded $validUntil must be ' . (self::HEX_ENCODED_VALID_UNTIL_LENGTH) . ' characters long');
        }

        $uuid = $this->getUuidFromEntity($entityOrUUID);
        return Base64::toUrlSaveBase64(hash_hmac('sha256', $uuid . $useFor . $validUntil, $this->key, true));
    }

    public function verifyToken($entityOrUUID, $token, $useFor, DateTimeInterface|int $validUntil = null): bool
    {
        return hash_equals($this->generateToken($entityOrUUID, $useFor, $validUntil), $token);
    }

    public function generateCombinedToken($entityOrUUID, string $useFor, DateTimeInterface|int $validUntil = null): string
    {
        $uuid = $this->getUuidFromEntity($entityOrUUID);
        $token = $this->generateToken($uuid, $useFor, $validUntil);
        $encodedUuid = UuidConverter::toBase64($uuid);
        if (strlen($encodedUuid) != self::BASE64_ENCODED_UUID_LENGTH) {
            throw new Exception('Sanity Check: there was en error generating the token. Encoded uuid is not the expected length');
        }
        if (strlen($token) != self::BASE64_ENCODED_HMAC_LENGTH && strlen($token) != self::BASE64_ENCODED_HMAC_LENGTH + self::HEX_ENCODED_VALID_UNTIL_LENGTH) {
            throw new Exception('Sanity Check: there was en error generating the token. Encoded hmac is not the expected length');
        }
        if ($validUntil instanceof DateTimeInterface) {
            $validUntil = $validUntil->getTimestamp();
        }
        $validUntil = $validUntil ? dechex($validUntil) : '';
        return $encodedUuid . $token . $validUntil;
    }

    public function loadEntityFromCombinedToken(string $token, string $entityClass, string $useFor): ?object
    {
        $len = self::BASE64_ENCODED_HMAC_LENGTH + self::BASE64_ENCODED_UUID_LENGTH + self::HEX_ENCODED_VALID_UNTIL_LENGTH;
        $validUntil = null;
        if (strlen($token) == $len) {
            $validUntil = hexdec(substr($token, -self::HEX_ENCODED_VALID_UNTIL_LENGTH));
            $token = substr($token, 0, -self::HEX_ENCODED_VALID_UNTIL_LENGTH);
        }
        $len -= self::HEX_ENCODED_VALID_UNTIL_LENGTH;
        if (strlen($token) != $len) {
            throw new InvalidArgumentException('$token must be ' . $len . ' characters long');
        }
        $uuid = UuidConverter::fromBase64(substr($token, 0, self::BASE64_ENCODED_UUID_LENGTH));
        $hmac = substr($token, self::BASE64_ENCODED_UUID_LENGTH);

        if (!$this->verifyToken($uuid, $hmac, $useFor, $validUntil)) {
            throw new AccessDeniedException();
        }

        if ($validUntil !== null && $validUntil < time()) {
            throw new AccessDeniedException('token is expired');
        }

        if(method_exists($entityClass, 'getUlid')) {
            $uuid = Ulid::fromRfc4122($uuid);
        }

        return $this->em->getRepository($entityClass)->find($uuid);
    }

    private function getUuidFromEntity(object|string $entityOrUUID): string
    {
        if (method_exists($entityOrUUID, 'getUlid')) {
            /** @var Ulid $ulid */
            $ulid = $entityOrUUID->getUlid();
            $uuid = $ulid->toRfc4122();
        } else if (method_exists($entityOrUUID, 'getId')) {
            $uuid = $entityOrUUID->getId();
        } else {
            $uuid = $entityOrUUID;
        }
        if (!UuidConverter::isUUID($uuid)) {
            throw new Exception('given value is not an uuid');
        }
        return $uuid;
    }
}
