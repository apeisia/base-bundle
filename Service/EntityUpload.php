<?php

namespace Apeisia\BaseBundle\Service;

abstract class EntityUpload
{
    public static function isVichInjection(): bool
    {
        foreach (debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS) as $bt) {
            if (array_key_exists('class', $bt) && $bt['class'] == \Vich\UploaderBundle\Mapping\PropertyMapping::class) {
                return true;
            }
        }
        return false;
    }

    public static function noVichInjection(): bool
    {
        return !self::isVichInjection();
    }
}
