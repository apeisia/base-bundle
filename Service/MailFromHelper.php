<?php

namespace Apeisia\BaseBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;

readonly class MailFromHelper
{
    public function __construct(
        private ContainerInterface $container,
    )
    {
    }

    public function getFromAddress(): string
    {
        if ($this->container->hasParameter('mail_from')) {
            return $this->container->getParameter('mail_from');
        }

        if (array_key_exists('MAIL_FROM', $_ENV)) {
            return $_ENV['MAIL_FROM'];
        }

        throw new \InvalidArgumentException('Missing $mailFrom argument and parameter "mail_from" or env MAIL_FROM does not exist.');
    }


    public function getMailSenderName(): string
    {
        if ($this->container->hasParameter('mail_sender')) {
            return $this->container->getParameter('mail_sender');
        }

        if (array_key_exists('MAIL_SENDER', $_ENV)) {
            return $_ENV['MAIL_SENDER'];
        }

        throw new \InvalidArgumentException('Missing $mailSender argument and parameter "mail_sender" or env MAIL_SENDER does not exist.');
    }
}
