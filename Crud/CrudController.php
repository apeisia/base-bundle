<?php

namespace Apeisia\BaseBundle\Crud;

use Apeisia\BaseBundle\Service\ObjectToArrayTransformer;
use Apeisia\BaseBundle\Service\VueForm\VueForm;
use Apeisia\BaseBundle\Service\VueForm\VueFormFactory;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Exception;
use FOS\RestBundle\View\View;
use Knp\Component\Pager\Pagination\AbstractPagination;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

abstract class CrudController extends AbstractController
{
    public static function getSubscribedServices(): array
    {
        return array_merge(parent::getSubscribedServices(), [
            'knp_paginator',
            VueFormFactory::class,
            EntityManagerInterface::class,
            ObjectToArrayTransformer::class,
        ]);
    }

    protected function createVueForm($entity): VueForm
    {
        return $this->container->get(VueFormFactory::class)->createVueForm($this->getForm($entity));
    }

    protected function getEntityManager(): EntityManagerInterface
    {
        return $this->container->get(EntityManagerInterface::class);
    }

    protected function getObjectToArrayTransformer(): ObjectToArrayTransformer
    {
        return $this->container->get(ObjectToArrayTransformer::class);
    }

    protected function doList(string   $entityClass,
                              Request  $request,
                              callable $filterAction = null,
                              array    $serializerGroups = ['list'],
                              callable $map = null,
                              bool     $distinct = true,
                              bool     $wrapQueries = false): View
    {
        $query        = $this->getListQuery($entityClass, $request->get('search'), $filterAction);
        $itemsPerPage = intval($request->get('itemsPerPage', 30));
        if ($itemsPerPage == -1) {
            $itemsPerPage = PHP_INT_MAX;
        }
        $data = $this->getPaginatedList($query, $request->get('page', 1), $itemsPerPage, $distinct, $wrapQueries);
        if ($map) {
            $serialized         = $this->getObjectToArrayTransformer()->toArray($data, $serializerGroups);
            $list               = $serialized['list'];
            $serialized['list'] = [];

            if (count($list) != count($data['list'])) {
                throw new \LogicException(sprintf('count of $list and $data does not match (list=%d, data=%d)', count($list), count($data)));
            }

            for ($i = 0; $i < count($list); $i++) {
                $item = $map($list[$i], $data['list'][$i]);
                if ($item) {
                    $serialized['list'][] = $item;
                }
            }

            $data = $serialized;
        }

        return $this->getSerializationView(
            $data,
            $serializerGroups,
        );
    }

    /**
     * @deprecated use doList() instead
     */
    protected function doIndex(Request $request, $filterAction = null, ?string $entityClass = null): View
    {
        return $this->doList($this->getEntityName(), $request, $filterAction);
    }

    protected function getPaginatedList($query, $page, $limit, bool $distinct = true, bool $wrapQueries = false): array
    {
        $paginator = $this->container->get('knp_paginator');
        /** @var AbstractPagination $pagination */
        $pagination = $paginator->paginate($query, $page, $limit, ['distinct' => $distinct, 'wrap-queries' => $wrapQueries]);

        return [
            'itemsPerPage' => $pagination->getItemNumberPerPage(),
            'page'         => $pagination->getCurrentPageNumber(),
            'pageCount'    => intval(ceil($pagination->getTotalItemCount() / $pagination->getItemNumberPerPage())),
            'totalItems'   => $pagination->getTotalItemCount(),
            'list'         => $pagination->getItems(),
        ];
    }

    protected function getFormSchema($entity = null): JsonResponse
    {
        $this->assertAccess($entity);

        return $this->createVueForm($this->getForm($entity))->createSchemaResponse();
    }

    protected function save(Request $request, $entity, $persistAction = null): View
    {
        $this->assertAccess($entity);

        return $this->createVueForm($entity)->save($request, $persistAction);
    }

    protected function getSerializationView($data, array $groups = [], $statusCode = null): View
    {
        $view = View::create($data, $statusCode);
        if ($groups) {
            $view->getContext()->setGroups($groups);
        }

        return $view;
    }

    protected function doDelete($entity)
    {
        $this->assertAccess($entity);
        $em = $this->getEntityManager();
        $em->remove($entity);
        $em->flush();
    }

    /**
     * @param string|null $search
     * @param callable|null $filterAction
     *
     * @return QueryBuilder
     * @throws Exception
     */
    final protected function getListQuery(string   $entityClass,
                                          string   $search = null,
                                          callable $filterAction = null): QueryBuilder
    {
        $repo = $this->getEntityManager()->getRepository($entityClass);
        if (!$repo instanceof SearchRepository) {
            throw new Exception('The Repository ' . $entityClass . ' must implement ' . SearchRepository::class);
        }
        $builder = $repo->getList($this->getListUser(), $search);
        $this->applyFilter($builder);
        if (is_callable($filterAction)) {
            $filterAction($builder);
        }

        return $builder;
    }

    protected function assertAccess($entity)
    {
    }

    /**
     * @deprecated Use filterAction parameter of doList() instead
     */
    protected function applyFilter(QueryBuilder $builder)
    {
    }

    protected function getListUser()
    {
        return $this->getUser();
    }

    /**
     * @deprecated Use doList() instead
     */
    protected function getEntityName()
    {
        throw new \Exception('getEntityName() and doIndex() are deprecated. Use doList() instead');
    }

    protected function getForm($entity): FormInterface
    {
        throw new \Exception('you must implement getForm to use form functions');
    }

}
