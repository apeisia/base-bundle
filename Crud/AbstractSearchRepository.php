<?php

namespace Apeisia\BaseBundle\Crud;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Orx;
use Doctrine\ORM\QueryBuilder;

abstract class AbstractSearchRepository extends EntityRepository implements SearchRepository
{
            use SearchRepositoryTrait;
}
