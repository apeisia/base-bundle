<?php

namespace Apeisia\BaseBundle\Crud;

/**
 * Alias for CrudController for backwards compatibility.
 *
 * @deprecated Use CrudController instead
 */
abstract class Controller extends CrudController
{

}
