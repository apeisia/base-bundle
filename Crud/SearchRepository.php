<?php

namespace Apeisia\BaseBundle\Crud;

interface SearchRepository {
    public function getList($user = null, $search = null);
}
