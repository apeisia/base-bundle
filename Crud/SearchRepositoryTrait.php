<?php

namespace Apeisia\BaseBundle\Crud;

use Doctrine\ORM\Query\Expr\Orx;
use Doctrine\ORM\QueryBuilder;

trait SearchRepositoryTrait
{
    protected function createSearchExpression(QueryBuilder $q, $search = null)
    {
        $terms = explode('|', $search);

        $exprs = [];
        foreach ($this->getSearchFields() as $f) {
            if (!str_contains($f, '.')) $f = 'a.' . $f;
            $innerExprs = [];
            foreach($terms as $i=>$term) {
                $innerExprs[] = $q->expr()->like($f, ':search'.$i);
            }
            $exprs[] = count($innerExprs) > 1 ? new Orx($innerExprs) : $innerExprs[0];
        }
        foreach($terms as $i=>$term) {
            $q->setParameter('search'.$i, '%' . trim($term) . '%');
        }
        return new Orx($exprs);
    }

    public function getList($user = null, $search = null)
    {
        $q = $this->createQueryBuilder('a');
        if ($search) {
            $q->where($this->createSearchExpression($q, $search));
        }

        return $q;
    }

    protected abstract function getSearchFields();
}
