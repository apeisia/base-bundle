<?php

namespace Apeisia\BaseBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Exception\InsufficientAuthenticationException;

class KernelResponseListener implements EventSubscriberInterface
{

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::EXCEPTION => 'onKernelException',
        ];
    }

    public function onKernelException(ExceptionEvent $event)
    {
        if (!$event->isMainRequest()) return;

        $e = $event->getThrowable();
        if ($e instanceof InsufficientAuthenticationException) {
            $event->setResponse(new Response($e->getMessage(), 401));
        }
    }
}
