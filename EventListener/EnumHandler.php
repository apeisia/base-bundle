<?php

namespace Apeisia\BaseBundle\EventListener;

//https://github.com/schmittjoh/serializer/issues/1373#issuecomment-1087737120
use JMS\Serializer\Context;
use JMS\Serializer\GraphNavigator;
use JMS\Serializer\Handler\SubscribingHandlerInterface;
use JMS\Serializer\JsonDeserializationVisitor;
use JMS\Serializer\JsonSerializationVisitor;

class EnumHandler implements SubscribingHandlerInterface
{
    public static function getSubscribingMethods(): array
    {
        return [
            [
                'direction' => GraphNavigator::DIRECTION_SERIALIZATION,
                'format' => 'json',
                'type' => 'enum',
                'method' => 'serializeEnumToJson',
            ],
            [
                'direction' => GraphNavigator::DIRECTION_DESERIALIZATION,
                'format' => 'json',
                'type' => 'enum',
                'method' => 'deserializeEnumFromJson',
            ],
        ];
    }

    public function serializeEnumToJson(JsonSerializationVisitor $visitor, \BackedEnum $data, array $type, Context $context): string|int
    {
        return $data->value;
    }

    /**
     * @template T
     *
     * @param array{params: array<array-key, array{name: class-string<T>}>} $type
     *
     * @return \BackedEnum|T
     */
    public function deserializeEnumFromJson(JsonDeserializationVisitor $visitor, mixed $data, array $type, Context $context)
    {
        /** @var ?class-string<T> $type */
        $type = $type['params'][0]['name'] ?? null;
        if (null === $type || !is_a($type, \BackedEnum::class, true)) {
            throw new \LogicException();
        }

        return $type::from($data);
    }
}
