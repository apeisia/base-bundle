<?php

namespace Apeisia\BaseBundle\FormType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EuroCentType extends AbstractType
{
    public function getParent(): string
    {
        return NumberType::class;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('attr', [
            'step' => '0.01',
            'data-convert-factor' => '100',
        ]);
        $resolver->setDefault('html5', true);
    }
}
