<?php

namespace Apeisia\BaseBundle\FormType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class QuillType extends AbstractType
{
    public function getBlockPrefix(): string
    {
        return 'quill';
    }

    public function getParent(): ?string
    {
        return TextareaType::class;
    }
}
