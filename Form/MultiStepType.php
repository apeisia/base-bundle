<?php

namespace Apeisia\BaseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MultiStepType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $i = 0;
        foreach ($options['steps'] as $step) {
            $builder
                ->add('step' . (++$i), get_class(new class extends AbstractType {
                        public function buildForm(FormBuilderInterface $builder, array $options): void
                        {
                            $options['step']($builder, $options);
                        }

                        public function configureOptions(OptionsResolver $resolver): void
                        {
                            $resolver->setDefaults([
                                'step' => null,
                            ]);
                        }
                    }
                ),
                    [
                        'step'         => $step,
                        'mapped'       => false,
                        'inherit_data' => true,
                        'data_class'   => $options['data_class'],
                    ]
                );
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefault('mapped', false);
        $resolver->setDefault('steps', []);
        $resolver->setDefault('inherit_data', true);
        $resolver->setAllowedTypes('steps', ['iterable']);
    }
}
