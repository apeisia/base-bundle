<?php

namespace Apeisia\BaseBundle\Form;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;

class PriceCentType extends NumberType
{

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm($builder, $options);
        $builder->addModelTransformer(new class implements DataTransformerInterface {
            public function transform($value): ?float
            {
                return $value / 100;
            }

            public function reverseTransform($value): ?int
            {
                return $value * 100;
            }
        });
    }

    public function getBlockPrefix(): string
    {
        return 'price_cent';
    }
}
