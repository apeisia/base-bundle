<?php

namespace Apeisia\BaseBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\ChoiceList\View\ChoiceView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EntityTypeExtension extends AbstractTypeExtension
{
    public static function getExtendedTypes(): iterable
    {
        return [EntityType::class];
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefined(['additional_data']);
    }

    public function buildView(FormView $view, FormInterface $form, array $options): void
    {

        if (isset($options['additional_data'])) {
            /** @var ChoiceView $choice */
            foreach ($view->vars['choices'] as $choice) {
                $choice->attr['additional_data'] = $options['additional_data']($choice->data);
            }
        }
    }
}
