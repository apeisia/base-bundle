<?php

namespace Apeisia\BaseBundle\MenuRouter;

class ExternalRoute extends BaseMenuRouterItem
{
    public function __construct(string $name, string $url, string $icon = 'fa fa-external-link')
    {
        $this->id   = $url;
        $this->name = $name;
        $this->icon = $icon;
    }

    public function getRouterConfigArray(): array
    {
        return [];
    }

    /**
     * Get the components menu configuration as an array.
     *
     * @return array
     */
    public function getMenuConfig(): array
    {
        return [
            'label'   => $this->getName(),
            'extlink' => $this->getId(),
            'icon'    => $this->getIcon(),
        ];
    }
}
