<?php

namespace Apeisia\BaseBundle\MenuRouter;

use Apeisia\BaseBundle\Exception\MenuElementException;

class MenuRouter extends BaseMenuRouterItem implements \Countable
{
    /**
     * The router's menu elements
     *
     * @var MenuRouterComponent[]
     */
    private array $children = [];

    /**
     * The position of new menu elements if no position is set.
     *
     * @var int
     */
    private int $nextAutoPosition = 0;

    /**
     * MenuRouter constructor.
     *
     * @param string $id
     * @param string|null $name
     * @param null $position
     */
    public function __construct(string $id, string $name = null, $position = null)
    {
        $this->id   = $id;
        $this->name = $name;
        $this->setPosition($position);
    }

    /**
     * Add a child
     *
     * @param MenuRouterComponent $child
     */
    public function addChild(MenuRouterComponent $child)
    {
        if ($child->getPosition() === null) {
            $child->setPosition(++$this->nextAutoPosition);
        }

        $child->setParent($this);

        $this->children[$child->getId()] = $child;
        uasort($this->children, fn($a, $b) => $a->getPosition() <=> $b->getPosition());
    }

    /**
     * Get a child element by its id. Throws an MenuElementException if element does not exist.
     *
     * @param string $id
     *
     * @return MenuRouterComponent
     * @throws MenuElementException
     *
     */
    public function getChild(string $id): MenuRouterComponent
    {
        if (array_key_exists($id, $this->children)) {
            return $this->children[$id];
        }

        throw new MenuElementException(sprintf('Menu has no direct child with id "%s".', $id));
    }

    /**
     * Get a child element by its id. Throws an MenuElementException if element does not exist or is not a menu.
     *
     * @param string $id
     *
     * @return MenuRouter
     * @throws MenuElementException
     *
     */
    public function getChildMenu(string $id): MenuRouter
    {
        $child = $this->getChild($id);

        if (!$child instanceof MenuRouter) {
            throw new MenuElementException(sprintf('Menu child with id "%s" is no menu.', $id));
        }

        return $child;
    }

    /**
     * Get the component as an array.
     *
     * @return array
     */
    public function getRouterConfigArray(): array
    {
        $elements = [];

        foreach ($this->children as $child) {
            if ($child instanceof self) {
                if (!$child->hasItemsRecursive()) {
                    continue;
                }

                // flatten submenus
                foreach ($child->getRouterConfigArray() as $subchild) {
                    $elements[] = $subchild;
                }
            } else {
                $elements[] = $child->getRouterConfigArray();
            }
        }

        return $elements;
    }

    /**
     * @return array
     */
    public function getMenuConfigRoot(): array
    {
        $elements = [];

        foreach ($this->children as $child) {
            if ($child instanceof self && $child->countChildren() === 0) {
                // ignore empty menus
                continue;
            }

            if ($child->isVisible()) {
                $elements[] = $child->getMenuConfig();
            }
        }

        return $elements;
    }

    public function getMenuConfig(): array
    {
        $elements = $this->getMenuConfigRoot();

        return [
            'id'       => $this->getId(),
            'label'    => $this->getName(),
            'icon'     => $this->getIcon(), // deprecated, use settings.icon instead
            'children' => $elements,
            'settings' => array_merge($this->settings, ['icon' => $this->getIcon()]),
        ];
    }

    public function count(): int
    {
        return count($this->children);
    }

    /** @deprecated use countChildren() instead */
    public function countChilds()
    {
    }

    public function countChildren()
    {
        $count = 0;
        foreach ($this->children as $child) {
            if ($child instanceof self) {
                $count += $child->countChildren();
            } else {
                ++$count;
            }
        }

        return $count;
    }

    public function hasItemsRecursive(): bool
    {
        foreach ($this->children as $child) {
            if ($child instanceof self) {
                if ($child->hasItemsRecursive()) {
                    return true;
                }
            } else {
                return true;
            }
        }

        return false;
    }
}
