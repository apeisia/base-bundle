<?php

namespace Apeisia\BaseBundle\MenuRouter;

interface MenuRouterComponent
{
    /**
     * Get a unique id, which can be used to get a specific element of the menu hierarchy.
     *
     * @return string
     */
    public function getId(): string;

    /**
     * Get the display name for this menu entry
     *
     * @return string
     */
    public function getName(): string;

    /**
     * Get an icon that should be displayed next to the menu entry.
     *
     * @return string
     */
    public function getIcon();

    /**
     * @return bool
     */
    public function isVisible(): bool;

    /**
     * Set the routers parent
     *
     * @param MenuRouterComponent $parent
     */
    public function setParent(MenuRouterComponent $parent);

    /**
     * Get the routers parent
     *
     * @return MenuRouterComponent
     */
    public function getParent(): MenuRouterComponent;

    /**
     * Get the component as an array.
     *
     * @return array
     */
    public function getRouterConfigArray(): array;

    /**
     * Get the components menu configuration as an array.
     *
     * @return array
     */
    public function getMenuConfig(): array;

    /**
     * The position of this element in the menu. Null if the position
     * should be determined automatically.
     *
     * @return int|null
     */
    public function getPosition(): ?int;

    public function setPosition(?int $position);
}
