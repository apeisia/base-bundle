<?php

namespace Apeisia\BaseBundle\MenuRouter;

trait SimpleCrudRoutingTrait
{
    private function addSimpleCrud(MenuRouter $router, $module, $menuName, $itemName, $path, $icon, $withWrite = true, $params = null, $position = null)
    {

        $translate = isset($this->useTranslation) ? $this->useTranslation : false;

        $router->addChild(new MenuRoute($module.'.index', $menuName, $path, $icon, null, null, $params, $position));
        $router->addChild(new MenuRoute(
            $module.'.show', $itemName . ($translate ? '.show' : ''),
            $path.'/:id', $icon, false, null, $params, $position
        ));
        if($withWrite) {
            $router->addChild(new MenuRoute($module . '.new', $itemName . ($translate ? '.new' : ' anlegen'),
                $path . '/new', $icon, false, $module . '.edit', $params, $position));
            $router->addChild(new MenuRoute($module . '.edit', $itemName . ($translate ? '.edit' :  ' bearbeiten'),
                $path . '/:id/edit', $icon, false, null, $params, $position));
        }
    }
}
