<?php

namespace Apeisia\BaseBundle\MenuRouter;

use Apeisia\BaseBundle\Event\ConfigureRouterEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class MenuRouterFactory
{
    public static function createRootMenu(EventDispatcherInterface $eventDispatcher, string $environment): MenuRouter
    {
        $root = new MenuRouter('root');

        if ($environment == 'dev') {

            // the profiler collects "not called listeners" in the kernel terminate event and thus fires this factory.
            // we do not want to run our router config event in the kernel terminate because that causes headaches with
            // missing request objects.
            // we look in our trace if we came from the profiler. because this is only in dev, we accept this for way now.

            $bt = debug_backtrace(false);

            foreach ($bt as $call) {
                if ($call['function'] == 'lateCollect') {
                    return $root;
                }
            }
        }

        $eventDispatcher->dispatch(new ConfigureRouterEvent($root));

        return $root;
    }
}
