<?php

namespace Apeisia\BaseBundle\MenuRouter;

class MenuRoute extends BaseMenuRouterItem
{
    /**
     * The url pattern for this route (aurelia's "route" property)
     *
     * @var string
     */
    private $pattern;

    /**
     * @var string
     */
    private $module;

    /**
     * @var array
     */
    private $moduleOptions = null;

    /**
     * @var array
     */
    private $params = null;

    /**
     * MenuRoute constructor.
     *
     * @param string $id
     * @param string $name
     * @param string $pattern
     * @param string|null $icon
     * @param null $visible
     * @param string $module
     */
    public function __construct(
        string $id,
        string $name,
        string $pattern,
        string $icon = null,
        $visible = null,
        string $module = null,
        $params = null,
        $position = null
    ) {
        $this->id   = $id;
        $this->name = $name;
        $this->setPattern($pattern);
        $this->setModule($module == null ? $id : $module);
        $this->icon = $icon;
        if ($visible === null) {
            $visible = $icon !== null; // display by default if icon is set
        }
        $this->setVisible($visible);
        $this->params = $params;
        $this->setPosition($position);
    }

    /**
     * @return string
     */
    public function getPattern(): string
    {
        return $this->pattern;
    }

    /**
     * @param string $pattern
     *
     * @return $this
     */
    public function setPattern(string $pattern)
    {
        $this->pattern = $pattern;

        return $this;
    }

    /**
     * @return string
     */
    public function getModule(): string
    {
        return $this->module;
    }

    /**
     * @param string $module
     *
     * @return $this
     */
    public function setModule(string $module)
    {
        $this->module = $module;

        return $this;
    }

    public function getParams(): ?array
    {
        return $this->params;
    }

    public function setParams(?array $params): self
    {
        $this->params = $params;

        return $this;
    }

    /**
     * @return string[]
     */
    public function getModuleOptions(): array
    {
        return $this->moduleOptions;
    }

    /**
     * @param string $option
     * @param string $value
     * @return $this
     *
     */
    public function setModuleOption(string $option, string $value)
    {
        $this->moduleOptions[$option] = $value;

        return $this;
    }

    public function getRouterConfigArray(): array
    {
        $r = [
            'path'     => $this->getPattern(),
            'route'    => $this->getPattern(), // TODO remove (aurelia)
            'name'     => $this->getId(),
            'moduleId' => $this->getModule(),
            'title'    => $this->getName(),
            'settings' => array_merge($this->settings, ['icon' => $this->getIcon()]),  // TODO remove (aurelia)
            'meta' => array_merge($this->settings, ['icon' => $this->getIcon()]),
        ];
        if ($this->moduleOptions) {
            $r['moduleOptions'] = $this->moduleOptions;
        }

        return $r;
    }

    public function getMenuConfig(): array
    {
        return [
            'label'    => $this->getName(),
            'route'    => $this->getId(),
            'icon'     => $this->getIcon(), // depecrecated, use settings.icon instead
            'params'   => $this->params,
            'settings' => array_merge($this->settings, ['icon' => $this->getIcon()]),
        ];
    }
}
