<?php

namespace Apeisia\BaseBundle\MenuRouter;

abstract class BaseMenuRouterItem implements MenuRouterComponent
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $icon;

    /**
     * Should this route be visible in the menu? (aurelia's "nav" property)
     *
     * @var bool
     */
    protected $visible = true;

    /**
     * The parent router (if any)
     *
     * @var MenuRouter
     */
    protected $parent = null;

    /**
     * Position of this element
     *
     * @var int|null
     */
    protected $position = null;

    /**
     * @var array
     */
    protected $settings = [];

    /**
     * Get a unique id, which can be used to get a specific element of the menu hierarchy.
     *
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * Get the display name for this menu entry
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Set the routers parent
     *
     * @param MenuRouterComponent $parent
     */
    public function setParent(MenuRouterComponent $parent)
    {
        $this->parent = $parent;
    }

    /**
     * Get the routers parent
     *
     * @return MenuRouterComponent
     */
    public function getParent(): MenuRouterComponent
    {
        return $this->parent;
    }

    /**
     * Get an icon that should be displayed next to the menu entry.
     *
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * @param string $icon
     *
     * @return $this
     */
    public function setIcon(string $icon)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * @return bool
     */
    public function isVisible(): bool
    {
        return $this->visible;
    }

    /**
     * @param bool $visible
     *
     * @return $this
     */
    public function setVisible(bool $visible)
    {
        $this->visible = $visible;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getPosition(): ?int
    {
        return $this->position;
    }

    /**
     * @param int|null $position
     * @return self
     */
    public function setPosition(?int $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function setSettings(array $settings): self
    {
        $this->settings = $settings;

        return $this;
    }

    public function setSetting($setting, $value): self
    {
        if (!is_array($this->settings)) {
            $this->settings = [];
        }

        $this->settings[$setting] = $value;

        return $this;
    }

    public function getSettings(): array
    {
        return $this->settings;
    }
}
