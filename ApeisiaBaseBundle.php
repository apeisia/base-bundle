<?php

namespace Apeisia\BaseBundle;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Apeisia\BaseBundle\DependencyInjection\ApeisiaBaseExtension;

class ApeisiaBaseBundle extends Bundle
{
    public function build(ContainerBuilder $container): void
    {
        $container->registerExtension(new ApeisiaBaseExtension());
    }
}
