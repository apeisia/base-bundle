<?php

namespace Apeisia\BaseBundle\Vich;

use Vich\UploaderBundle\Handler\UploadHandler as BaseHandler;

// add to base handler: remove file when file is changed to null
class UploadHandler extends BaseHandler
{
//    public function clean($obj, string $fieldName): void
//    {
//        $mapping = $this->getMapping($obj, $fieldName);
//
//        if ($this->hasUploadedFile($obj, $mapping) || $mapping->getFile($obj) === null) {
//            $this->remove($obj, $fieldName);
//            $this->getMapping($obj, $fieldName)->writeProperty($obj, 'name', null);
//        }
//    }
}
