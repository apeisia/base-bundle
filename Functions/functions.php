<?php

/**
 * @param array|iterable $input
 * @param callable<item> $callback
 * @return array
 */
function map($input, callable $callback)
{
    if ($input instanceof Traversable) {
        $input = iterator_to_array($input);
    }
    return array_values(array_map($callback, $input));
}

/**
 * @param array|iterable $input
 * @param callable<item> $callback
 * @return array
 */
function filter($input, callable $callback)
{
    if ($input instanceof Traversable) {
        $input = iterator_to_array($input);
    }
    return array_values(array_filter($input, $callback));
}

/**
 * @param array|iterable $input
 * @param callable<item> $callback
 * @return array
 */
function find($input, callable $callback)
{
    $output = filter($input, $callback);
    if (count($output) > 0) {
        return $output[0];
    }
    return null;
}

/**
 * @param array|iterable $input
 * @param mixed $initial
 * @param callable<carry, item> $callback
 * @return mixed
 */
function reduce($input, $initial, callable $callback)
{
    if ($input instanceof Traversable) {
        $input = iterator_to_array($input);
    }
    return array_reduce($input, $callback, $initial);
}
