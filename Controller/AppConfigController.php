<?php

namespace Apeisia\BaseBundle\Controller;

use Apeisia\BaseBundle\Service\AppConfigService;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AppConfigController extends AbstractController
{

    /**
     * @Get("/api/_app_config.json")
     */
    #[Get('/api/_app_config.json')]
    public function appConfigAction(AppConfigService $appConfigService): View
    {
        $view = View::create($appConfigService->getAppConfig());
        $view->getContext()->setGroups(['appConfig']);

        return $view;
    }
}
