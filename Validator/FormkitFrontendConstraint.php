<?php

namespace Apeisia\BaseBundle\Validator;

use Symfony\Component\Validator\Constraint;

class FormkitFrontendConstraint extends Constraint
{
    public function __construct(private readonly string $formkitValidator, private readonly string $formkitValidatorParam, private readonly ?string $message = null)
    {
        parent::__construct();
    }

    public function getFormkitValidator(): string
    {
        return $this->formkitValidator;
    }

    public function getFormkitValidatorParam(): string
    {
        return $this->formkitValidatorParam;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }
}