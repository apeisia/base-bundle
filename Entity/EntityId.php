<?php

namespace Apeisia\BaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Ramsey\Uuid\Doctrine\UuidGenerator;

trait EntityId
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    #[
        ORM\Column(name:"id", type:"integer"),
        ORM\Id,
        ORM\GeneratedValue(strategy: "AUTO"),
        ORM\CustomIdGenerator(class: UuidGenerator::class)
    ]
    #[Serializer\Groups(["Default", "list", "appConfig", "select", "id"])]
    protected $id;

    public function getId(): ?int
    {
        return $this->id;
    }
}
