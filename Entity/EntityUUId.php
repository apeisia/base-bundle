<?php

namespace Apeisia\BaseBundle\Entity;

use Apeisia\AccessorTraitBundle\Annotation as GetSet;
use Apeisia\BaseBundle\Generators\SetUuidGenerator;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

trait EntityUUId
{
    #[
        ORM\Column(type: "guid"),
        ORM\Id,
        ORM\GeneratedValue(strategy: "CUSTOM"),
        ORM\CustomIdGenerator(class: SetUuidGenerator::class)
    ]
    #[Serializer\Groups(["Default", "default", "list", "appConfig", "select", "id"])]
    #[Serializer\Expose]
    #[GetSet\None()]
    protected ?string $id = null;

    public function getId(): ?string
    {
        return $this->id;
    }
}
