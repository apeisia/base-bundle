<?php

namespace Apeisia\BaseBundle\Entity;

trait EntityUUIdSet
{
    public function setId(?string $id): void
    {
        $this->id = $id;
    }
}
