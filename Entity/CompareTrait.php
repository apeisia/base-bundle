<?php

namespace Apeisia\BaseBundle\Entity;

trait CompareTrait
{
    public function is($other)
    {
        if (!$other)
            return false;
        return ($other instanceof $this || $this instanceof $other) && $other->getId() == $this->getId();
    }
}
