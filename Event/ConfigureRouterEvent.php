<?php

namespace Apeisia\BaseBundle\Event;

use Apeisia\BaseBundle\MenuRouter\MenuRouter;
use Symfony\Contracts\EventDispatcher\Event;

class ConfigureRouterEvent extends Event
{
    private MenuRouter $router;

    public function __construct(MenuRouter $router)
    {
        $this->router = $router;
    }

    public function getRouter(): MenuRouter
    {
        return $this->router;
    }
}
