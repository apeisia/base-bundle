<?php

namespace Apeisia\BaseBundle\Event;


use Symfony\Contracts\EventDispatcher\Event;

class AppConfigEvent extends Event
{
    private array $config = [];

    public function getConfig(): array
    {
        return $this->config;
    }

    public function setConfig(string $key, $value)
    {
        $this->config[$key] = $value;
    }
}
