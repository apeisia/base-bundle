<?php

namespace Apeisia\BaseBundle\Filtration;

use Doctrine\ORM\Query;
use Knp\Component\Pager\Event\ItemsEvent;
use Knp\Component\Pager\Event\Subscriber\Filtration\Doctrine\ORM\Query\WhereWalker;
use Knp\Component\Pager\Event\Subscriber\Paginate\Doctrine\ORM\Query\Helper as QueryHelper;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class QuerySubscriber implements EventSubscriberInterface
{
    public function items(ItemsEvent $event)
    {
        if ($event->target instanceof Query) {
            // knp accesses $_GET directly from subscribers. we do the same
            $filter = [];
            if (isset($_GET['filter']) && is_array($_GET['filter'])) {
                foreach ($_GET['filter'] as $field => $value) {
                    if (is_string($field) && (is_string($value) || is_numeric($value))) {
                        $filter[$field] = str_replace('*', '%', $value);
                    }
                }
            }
            $event->target->setHint(FilterWalker::HINT_APEISIA_FILTER_ARRAY, $filter);
            QueryHelper::addCustomTreeWalker($event->target, FilterWalker::class);
        }
    }

    public static function getSubscribedEvents(): array
    {
        return [
            'knp_pager.items' => ['items', 0],
        ];
    }
}
