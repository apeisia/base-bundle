<?php
namespace Apeisia\BaseBundle\Filtration;

use Knp\Component\Pager\Event\BeforeEvent;
use Knp\Component\Pager\Event\Subscriber\Filtration\PropelQuerySubscriber;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class FiltrationSubscriber implements EventSubscriberInterface
{
    /**
     * Lazy-load state tracker
     * @var bool
     */
    private $isLoaded = false;

    public function before(BeforeEvent $event)
    {
        // Do not lazy-load more than once
        if ($this->isLoaded) {
            return;
        }

        $disp = $event->getEventDispatcher();
        // hook all standard filtration subscribers
        $disp->addSubscriber(new QuerySubscriber());

        $this->isLoaded = true;
    }

    public static function getSubscribedEvents(): array
    {
        return array(
            'knp_pager.before' => array('before', 1),
        );
    }
}
