<?php

namespace Apeisia\BaseBundle\Generators;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Id\AbstractIdGenerator;
use Doctrine\ORM\Mapping\Entity;
use Exception;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class SetUuidGenerator extends AbstractIdGenerator
{
    /**
     * Generate an identifier if the given entity doesn't have an id already.
     *
     * @param EntityManager $em
     * @param Entity $entity
     *
     * @return UuidInterface
     *
     * @throws Exception
     */
    public function generate(EntityManager $em, $entity)
    {
        if (method_exists($entity, 'getId') && $entity->getId() !== null) {
            return $entity->getId();
        }

        return Uuid::uuid4();
    }
}

