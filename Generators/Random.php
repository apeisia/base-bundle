<?php

namespace Apeisia\BaseBundle\Generators;


abstract class Random
{

    public static function generate($characters, $length)
    {
        $numCharacters = strlen($characters);
        $out           = '';
        for ($i = 1; $i <= $length; ++$i) {
            $out .= $characters[random_int(0, $numCharacters) - 1];
        }

        return $out;
    }

    public static function password()
    {
        return self::generate('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-', 10);
    }

    public static function alphanumeric($length)
    {
        return self::generate('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789', $length);
    }
}
