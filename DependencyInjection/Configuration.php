<?php

namespace Apeisia\BaseBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder(): \Symfony\Component\Config\Definition\Builder\TreeBuilder
    {
        $treeBuilder = new TreeBuilder('apeisia_base');
        $rootNode    = $treeBuilder->getRootNode();

        // @formatter:off
        $rootNode
            ->children()
                ->scalarNode('signed_entity_key')->defaultNull()->end()
                ->enumNode('schema_generator')
                    ->values(['vf', 'formkit'])
                    ->defaultValue('formkit')
            ->end();
        ;
        // @formatter:on

        return $treeBuilder;
    }
}
