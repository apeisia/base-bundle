<?php

namespace Apeisia\BaseBundle\DependencyInjection;

use Apeisia\BaseBundle\Service\EncryptAndMacService;
use Apeisia\BaseBundle\Service\SignedEntityToken;
use Apeisia\BaseBundle\Service\VueForm\FormKitSchema;
use Apeisia\BaseBundle\Service\VueForm\FormSchemaGenerator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;
use Apeisia\BaseBundle\Service\VueForm\SchemaGeneratorInterface;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class ApeisiaBaseExtension extends Extension
{
    /**
     * @param array $configs
     * @param ContainerBuilder $container
     * @throws \Exception
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config        = $this->processConfiguration($configuration, $configs);


        
        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yaml');

        //$this->registerRepositoriesAsServices($container);

        $container->register(SignedEntityToken::class)
            ->setArgument('$em', new Reference(EntityManagerInterface::class))
            ->setArgument('$key', $config['signed_entity_key'] ?? null);
        $container->register(EncryptAndMacService::class)
            ->setArgument('$key', $config['signed_entity_key'] ?? null);

        $schemaGeneratorClasses = [
            'formkit' => FormKitSchema::class,
            'vf' => FormSchemaGenerator::class,
        ];

        $container->setAlias(SchemaGeneratorInterface::class, $schemaGeneratorClasses[$config['schema_generator']]);
    }

//    /**
//     * @param ContainerBuilder $container
//     * @return mixed
//     */
//    private function registerRepositoriesAsServices(ContainerBuilder $container)
//    {
//        $bundleRoot = $container->getParameter('kernel.project_dir') . '/src';
//        foreach (new \DirectoryIterator($bundleRoot) as $bundleIt) {
//            if ($bundleIt->isDot()) continue;
//            $bundle = $bundleIt->getFilename();
//            if (!preg_match('/[a-zA-Z]+Bundle/', $bundle)) continue;
//            $repoRoot = $bundleRoot . '/' . $bundle . '/Repository';
//            if (!file_exists($repoRoot)) continue;
//            foreach (new \DirectoryIterator($repoRoot) as $repoIt) {
//                if ($repoIt->isDot()) continue;
//                if (!preg_match('/([a-zA-Z]+)Repository/', $repoIt->getFilename(), $matches)) continue;
//                [, $entity] = $matches;
//                $repo = $bundle . '\\Repository\\' . $entity . 'Repository';
//
//                $container->register($repo)
//                    ->setArguments([$bundle . '\\Entity\\' . $entity])
//                    ->setFactory([new Reference('doctrine.orm.entity_manager'), 'getRepository']);
//            }
//        }
//    }
}
