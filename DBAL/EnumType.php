<?php

namespace Apeisia\BaseBundle\DBAL;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

abstract class EnumType extends Type {

	/**
	 * @param array $fieldDeclaration
	 * @param AbstractPlatform $platform
	 * @return string
	 */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)    {
        return "ENUM('".implode("','", $this->getValues())."') COMMENT '(DC2Type:".$this->getName().")'";
    }

	/**
	 * @param mixed $value
	 * @param AbstractPlatform $platform
	 * @return mixed
	 */
    public function convertToPHPValue($value, AbstractPlatform $platform) {
        return $value;
    }

	/**
	 * @param mixed $value
	 * @param AbstractPlatform $platform
	 * @return mixed
	 */
    public function convertToDatabaseValue($value, AbstractPlatform $platform) {
        if (!in_array($value, $this->getValues())) {
            throw new \InvalidArgumentException('The value "'.$value.'" is not in enum array ("'.implode("', '", $this->getValues()).'") of type '.$this->getName());
        }
        return $value;
    }

	/**
	 * @return string[]
	 */
    abstract protected function getValues();

	/**
	 * @param AbstractPlatform $platform
	 * @return bool
	 */
    public function requiresSQLCommentHint(AbstractPlatform $platform) {
        return true;
    }
}
